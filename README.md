OpenYCT
=======

A free, libre, open-source Android app for practicing chinese characters, pinyin and pronunciation of the characters of the Youth Chinese Test, a HSK-like test for younger students.